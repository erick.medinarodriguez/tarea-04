// OPERADORES LOGICOS


//    NOT
/*
let numero1 = 123
let numero2 = 123

let sonIguales = numero1 == numero2

let sonDiferentes = !sonIguales

console.log(sonDiferentes)
*/


//   AND
/*
let estaLloviendo = true
let hayViento = false

console.log(estaLloviendo && !hayViento)
*/


//   OR

let nombre = "Juan"
let edad = 21

console.log((nombre == "Juan") || ((edad % 2) == 0))

