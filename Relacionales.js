// <, <=, >, >=, ==, ===

//menor que
let numero = 5;

if (numero< 7){
    console.log('${numero} es menor a 7')
} else{

    console.log('${numero}no es menor a 7')
}


//menor o igual que
if (numero <= 5){
    console.log('${numero} es menor o igual a 5')
} else{

    console.log('${numero}no es menor o igual a 5')
}

//mayor que

if (numero > 7){
    console.log('${numero} es mayor a 7')
} else{
    console.log('${numero}no es mayor a 7')
}

//mayor o igual que

if (numero >= 5){
    console.log('${numero} es mayor a 5')
} else{
    console.log('${numero}no es mayor a 5')
}

//igualdad
console.log(numero == 5)

console.log(numero == '5')

//igual estricto

console.log(numero === 5)

console.log(numero === '5')

// numero distinto

console.log(numero !=5)